'''
Utils Class - JSON conversion. HTTPS Calls and other useful methods
All enpoint classes use methods in this class so be careful when making changes here
'''

import json
import requests
from __configs import *

def str_json_to_dict(str_json):
    json_dict = json.loads(str_json)
    return json_dict

def dict_to_json(dictionary):
    json_dict = json.dumps(dictionary)
    return json_dict

def unverified_https_call(url,payload):
    print("Making Unverified Call - HTTPS is still used but certificate is not trusted")


def verified_https_call(method,path,payload=""):
    response = requests.request(method,GLOBAL_URL+path,data=payload, headers=HEADERS)
    return response


def verify_json(json_to_verify):
    print(json_to_verify)
