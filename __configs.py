import os

'''

'''


EMAIL = "support@reekoh.com"
BETA_URL = "https://beta-api.reekoh.io"
PROD_URL = "https://api.reekoh.io"
GLOBAL_URL = PROD_URL
ALLOW_NON_VERIFIED_CALLS = True


PASSWORD = os.environ.get("REEKOH_PASSWORD")
EMAIL= os.environ.get("REEKOH_EMAIL")

HEADERS = {}
HEADERS["content-type"] = "application/json"
HEADERS["accept"] = "application/json"
