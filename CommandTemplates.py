from __utils import * 


class CommandTemplates:

	def search(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="GET",path="/command-templates",payload=payload_json) 
 
	def read(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="GET",path="/command-templates/" + id) 
 
	def create(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="POST",path="/command-templates",payload=payload_json) 
 
	def delete(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="DELETE",path="/command-templates/" + id) 
 
