import requests
import json
import os


from __configs import *

'''

A Class for Working with the API in General. Methods include creating a scaffold from API endpoints, self describing APIs.

'''

def get_description_json():
    response = requests.request("GET",GLOBAL_URL,headers=HEADERS)
    json_resp = json.loads(response.text)
    return json_resp

def describe_api():
    json_resp = get_description_json()
    print("\r\n{}".format(json_resp['name']))
    print("{}".format(json_resp['description']))
    print("For any issues/comments please contact: {}".format(json_resp['email']))

    for api_endpoint in json_resp['_links']:
        print(api_endpoint)
        print("\t Method: {}  Path: {}{} \r\n".format(json_resp['_links'][api_endpoint]['method'],GLOBAL_URL,json_resp['_links'][api_endpoint]['path']))

def describe_major_enpoints():
    major_enpoints = set()
    json_resp = get_description_json()
    for api_endpoint in json_resp['_links']:
        api_endpoint_split = api_endpoint.split('.')
        major_enpoints.add(api_endpoint_split[0])
    return major_enpoints

def scaffold_endpoint_files(delete_existing):
    if delete_existing:
        delete_endpoint_files()
    endpoints_set = describe_major_enpoints()
    for endpoint in endpoints_set:
        filename = "{}.py".format(endpoint.title().replace("-",""))
        file = open(filename,'a')
        file.write("from __utils import * \r\n")
        file.write("\r\n\r\n")
        file.write("class {}:\r\n\r\n".format((endpoint).title().replace("-","")))
        file.close()

def delete_endpoint_files():
    endpoints_set = describe_major_enpoints()
    for endpoint in endpoints_set:
        filename = "{}.py".format().title()
        os.remove(filename)

def scaffold_endpoint_methods(api_endpoint,api_desc):
        print(api_endpoint)
        print(api_desc)
        print("\r \n")

        deal_with_id = False
        api_endpoint_split = api_endpoint.split('.')
        filename = "{}.py".format(api_endpoint_split[0].title().replace("-",""))

        if ":id" in api_desc['path']:
            deal_with_id = True

        file = open(filename,'a')

        if deal_with_id:
            file.write("\tdef {}(id): \r\n\r\n".format(api_endpoint_split[1].replace("-","_")))
        else:
            file.write("\tdef {}(payload_json=\"\"): \r\n\r\n".format(api_endpoint_split[1].replace("-","")))

        file.write("# API Version Used in Scaffold: {} \r\n".format(api_desc['version']))
        file.write("# API Versions Available: {} \r\n \r\n".format(api_desc['versions']))

        if deal_with_id:
            file.write("\t\tverified_https_call(method=\"{}\",path=\"{}\" + id) \r\n ".format(api_desc['method'],api_desc['path'].replace(":id","")))
        else:
            file.write("\t\treturn verified_https_call(method=\"{}\",path=\"{}\",payload=payload_json) \r\n ".format(api_desc['method'],api_desc['path']))

        file.write("\r\n")
        file.close()

def scaffold_all_endpoint_methods():
    api_endpoints = get_description_json()

    for api_endpoint in api_endpoints['_links']:
        scaffold_endpoint_methods(api_endpoint,api_endpoints['_links'][api_endpoint])
