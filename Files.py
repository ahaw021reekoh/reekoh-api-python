from __utils import * 


class Files:

	def search(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="GET",path="/files",payload=payload_json) 
 
	def read(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="GET",path="/files/" + id) 
 
	def create(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="POST",path="/files",payload=payload_json) 
 
