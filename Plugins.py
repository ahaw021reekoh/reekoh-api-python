from __utils import * 


class Plugins:

	def search(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="GET",path="/plugins",payload=payload_json) 
 
	def read(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="GET",path="/plugins/" + id) 
 
	def create(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="POST",path="/plugins",payload=payload_json) 
 
	def update(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="PATCH",path="/plugins",payload=payload_json) 
 
