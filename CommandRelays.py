from __utils import * 


class CommandRelays:

	def search(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="GET",path="/command-relays",payload=payload_json) 
 
	def read(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="GET",path="/command-relays/" + id) 
 
