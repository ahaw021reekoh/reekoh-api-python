from __utils import * 


class PipelineDrafts:

	def search(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="GET",path="/pipeline-drafts",payload=payload_json) 
 
	def read(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="GET",path="/pipeline-drafts/" + id) 
 
	def create(payload_json=""): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		return verified_https_call(method="POST",path="/pipeline-drafts",payload=payload_json) 
 
	def update(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="PATCH",path="/pipeline-drafts/" + id) 
 
	def delete(id): 

# API Version Used in Scaffold: 1.0.0 
# API Versions Available: ['1.0.0'] 
 
		verified_https_call(method="DELETE",path="/pipeline-drafts/" + id) 
 
