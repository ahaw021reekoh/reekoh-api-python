import requests
import json

from __configs import *
from Users import *

class Authenticator:

    '''
    Method for Getting Auth Tokens - Means we do not have to hard write Tokens
    Requires an environmental variable to be set with password and email
    Alternatively you can hard code a password (PASSWORD) in the configs.py file
    '''

    def __init__(self):
        print("Created Authenticator Object")
        self.email = EMAIL
        self.password = PASSWORD
        self.token = ""
        self.token_expiry = ""

    def get_auth_token(self):
        payload_dict = {}
        payload_dict["email"] = self.email
        payload_dict["password"] = self.password
        payload_json = json.dumps(payload_dict)
        response = Users.auth(payload_json)
        json_resp = json.loads(response.text)
        HEADERS['Authorization'] = "Bearer {}".format(json_resp['idToken'])
        self.token = json_resp['idToken']
        self.token_expiry = json_resp['expiry']

    def __str__(self):
        return "Current Token is: {} \r\n \t Current Token Expiry is: {}".format(self.token,self.token_expiry)

    def get_token(self):
        return self.token

    def get_expiry(self):
        return self.token_expiry

    def renew_token(sef):
        response = Users.refreshtoken()
        print(response.text)
        json_resp = json.loads(response.text)
        HEADERS['Authorization'] = "Bearer {}".format(json_resp['idToken'])
        self.token = json_resp['idToken']
        self.token_expiry = json_resp['expiry']
